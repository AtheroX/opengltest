#ifndef EBO_CLASS_H
#define EBO_CLASS_H

#include <glad/glad.h>
#include <vector>

class EBO {
public:
	EBO(std::vector<GLuint>& apIndices);

	void Bind();

	void Unbind();

	void Delete();


	GLuint mID;
};


#endif