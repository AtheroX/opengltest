#include "VAO.h"

VAO::VAO()
{
	GLCall(glGenVertexArrays(1, &mID));
}

void VAO::LinkAttrib(VBO& aVBO, GLuint aLayout, GLuint aNumComponents, GLenum aType, GLsizeiptr aStride, void* apOffset)
{
	aVBO.Bind();

	//Configure how to read the VBO and enables it (0 because is the first one)
	GLCall(glVertexAttribPointer(aLayout, aNumComponents, aType, GL_FALSE, aStride, apOffset));
	GLCall(glEnableVertexAttribArray(aLayout));

	aVBO.Unbind();
}

void VAO::Bind()
{
	GLCall(glBindVertexArray(mID));
}

void VAO::Unbind()
{
	GLCall(glBindVertexArray(0));
}

void VAO::Delete()
{
	GLCall(glDeleteVertexArrays(1, &mID));
}