#include "VBO.h"

VBO::VBO(std::vector<Vertex>& apVertices)
{
	GLCall(glGenBuffers(1, &mID));
	//Binds VBO as an array buffer and adds Vertices object to it
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, mID));
	GLCall(glBufferData(GL_ARRAY_BUFFER, apVertices.size() * sizeof(Vertex), apVertices.data(), GL_STATIC_DRAW));

}

void VBO::Bind() 
{
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, mID));
}

void VBO::Unbind() 
{
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, 0));
}

void VBO::Delete() 
{
	GLCall(glDeleteBuffers(1, &mID));
}
