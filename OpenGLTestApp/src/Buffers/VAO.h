#ifndef VAO_CLASS_H
#define VAO_CLASS_H

#include "VBO.h"

class VAO
{
public:
	VAO();

	void LinkAttrib(VBO& aVBO, GLuint aLayout, GLuint aNumComponents, GLenum aType, GLsizeiptr aStride, void* apOffset);

	void Bind();

	void Unbind();

	void Delete();


	GLuint mID;
};


#endif