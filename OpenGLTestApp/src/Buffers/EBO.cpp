#include "EBO.h"
#include "../AtheroXUtils.h"

EBO::EBO(std::vector<GLuint>& apIndices) {
	GLCall(glGenBuffers(1, &mID));
	//Binds EBO as an array buffer and adds Vertices object to it
	GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mID));
	GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, apIndices.size()*sizeof(GLuint), apIndices.data(), GL_STATIC_DRAW));

}

void EBO::Bind() {
	GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mID));
}

void EBO::Unbind() {
	GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
}

void EBO::Delete() {
	GLCall(glDeleteBuffers(1, &mID));
}
