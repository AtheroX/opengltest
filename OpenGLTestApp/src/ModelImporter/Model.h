﻿#ifndef CLASS_MODEL_H
#define CLASS_MODEL_H

#include <json/json.h>
#include "Mesh.h"

using json = nlohmann::json;

class Model
{
public:
	Model(const char* aFile,
		glm::vec3 aPos = glm::vec3(0.f),
		glm::quat aRot = glm::quat(1.0f, 0.0f, 0.0f, 0.0f),
		glm::vec3 aScale = glm::vec3(1.f));

	void Draw(Shader& aShader, Camera& aCamera);

	void Translate(glm::vec3 aPos);
	void Rotate(glm::vec3 aDegrees);
	void Scale(glm::vec3 aScale);

	
	glm::vec3 mPosition {1.0f};
	glm::quat mRotation {1.0f, 0.0f, 0.0f, 0.0f};
	glm::vec3 mScale {1.0f};

private:
	std::vector<unsigned char> GetData();

	std::vector<float> GetFloats(json aAccesor);
	std::vector<GLuint> GetIndices(json aAccesor);
	std::vector<Texture> GetTextures();
	std::vector<glm::vec2> GroupFloatsInVec2(std::vector<float> aFloatVect);
	std::vector<glm::vec3> GroupFloatsInVec3(std::vector<float> aFloatVect);
	std::vector<glm::vec4> GroupFloatsInVec4(std::vector<float> aFloatVect);

	std::vector<Vertex> AssembleVertices(std::vector<glm::vec3> aPositions, std::vector<glm::vec3> aNormals, std::vector<glm::vec3> aColors, std::vector<glm::vec2> aTexUVs);
	void LoadMesh(unsigned int mIndexMesh);

	void TraverseNode(unsigned int aNextNode, glm::mat4 aMatrix = glm::mat4(1.0f));
	
	
	const char* mFile;
	std::vector<unsigned char> mData;
	json mJSON;

	std::vector<std::string> mLoadedTexNames;
	std::vector<Texture> mLoadedTex;

	std::vector<Mesh> mMeshes;
	std::vector<glm::vec3> mTranslationMeshes;
	std::vector<glm::quat> mRotationMeshes;
	std::vector<glm::vec3> mScaleMeshes;
	std::vector<glm::mat4> mMatrixMeshes;
};

#endif
