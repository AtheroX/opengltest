﻿#include "Model.h"

Model::Model(const char* aFile, glm::vec3 aPos, glm::quat aRot, glm::vec3 aScale)
	: mFile(aFile), mPosition(aPos), mRotation(aRot), mScale(aScale)
{
	std::string Text = GetFileContents(aFile);
	mJSON = json::parse(Text);

	mData = GetData();

	TraverseNode(0);
}

void Model::Translate(glm::vec3 aPos)
{
	aPos.y*=-1.f;
	
	for(auto& t : mTranslationMeshes)
	{
		t += aPos;
	}
	
	mPosition+=aPos;
}

void Model::Rotate(glm::vec3 aDegrees)
{
	const auto NewQuat {glm::quat(aDegrees)};
	for(auto& r : mRotationMeshes)
	{
		r = rotate(r,glm::radians(aDegrees.x),glm::vec3(1,0,0));//rotation x = 0.0 degrees
		r = rotate(r,glm::radians(aDegrees.y),glm::vec3(0,1,0));//rotation y = 0.0 degrees
		r = rotate(r,glm::radians(aDegrees.z),glm::vec3(0,0,1));//rotation z = 0.0 degrees

	}
	
	mRotation = NewQuat;
}

void Model::Scale(glm::vec3 aScale)
{
	for(auto& s : mScaleMeshes)
	{
		s *= aScale;
	}
	
	mScale *= aScale;
}

void Model::LoadMesh(unsigned mIndexMesh)
{
	unsigned int PosAccessorIndex {mJSON["meshes"][mIndexMesh]["primitives"][0]["attributes"]["POSITION"]};
	unsigned int NormalAccessorIndex {mJSON["meshes"][mIndexMesh]["primitives"][0]["attributes"]["NORMAL"]};
	int ColorAccessorIndex {mJSON["meshes"][mIndexMesh]["primitives"][0]["attributes"].value("COLOR_0",-1)};
	int TexAccessorIndex {mJSON["meshes"][mIndexMesh]["primitives"][0]["attributes"].value("TEXCOORD_0",-1)};
	unsigned int IndicesAccessorIndex {mJSON["meshes"][mIndexMesh]["primitives"][0]["indices"]};

	std::vector<float> PosVec {GetFloats(mJSON["accessors"][PosAccessorIndex])};
	std::vector<glm::vec3> Pos {GroupFloatsInVec3(PosVec)};
	
	std::vector<float> NormalVec {GetFloats(mJSON["accessors"][NormalAccessorIndex])};
	std::vector<glm::vec3> Normals {GroupFloatsInVec3(NormalVec)};
	
	std::vector<glm::vec2> TexUvs(Pos.size());
	if(TexAccessorIndex != -1)
	{
		std::vector<float> TexVec {GetFloats(mJSON["accessors"][TexAccessorIndex])};
		TexUvs = GroupFloatsInVec2(TexVec);
	}
	
	std::vector<glm::vec3> Colors (Pos.size());
	if(ColorAccessorIndex != -1)
	{
		std::vector<float> ColorVec {GetFloats(mJSON["accessors"][ColorAccessorIndex])};
		Colors = GroupFloatsInVec3(ColorVec);
	}
	
	std::vector<Vertex> Vertices {AssembleVertices(Pos, Normals, Colors, TexUvs)};
	std::vector<GLuint> Indices {GetIndices(mJSON["accessors"][IndicesAccessorIndex])};
	std::vector<Texture> Textures {GetTextures()};

	mMeshes.push_back({Vertices, Indices, Textures});
}

std::vector<unsigned char> Model::GetData()
{
	std::string ByteText;
	std::string Uri{mJSON["buffers"][0]["uri"]};

	std::string FileString{mFile};
	std::string FileDir{FileString.substr(0, FileString.find_last_of("/") + 1)};
	ByteText = GetFileContents((FileDir + Uri).c_str());

	std::vector<unsigned char> Data{ByteText.begin(), ByteText.end()};
	return Data;
}

std::vector<float> Model::GetFloats(json aAccesor)
{
	std::vector<float> FloatVect;

	int BuffViewIndex{aAccesor.value("bufferView", 1)};
	int Count{aAccesor["count"]};
	int AccByteOffset{aAccesor.value("byteOffset", 0)};
	std::string Type{aAccesor["type"]};

	json BufferView{mJSON["bufferViews"][BuffViewIndex]};
	int ByteOffset = BufferView["byteOffset"];
	int NumPerVert;

	if(Type == "SCALAR") NumPerVert = 1;
	else if(Type == "VEC2") NumPerVert = 2;
	else if(Type == "VEC3") NumPerVert = 3;
	else if(Type == "VEC4") NumPerVert = 4;
	else throw std::invalid_argument("[MODEL PARSER] Type is not SCALAR | VEC2 | VEC3 | VEC4");

	int BeginOfData = ByteOffset + AccByteOffset;
	int LengthOfData = Count * 4 * NumPerVert;

	for (int CurrData {BeginOfData}; CurrData < BeginOfData + LengthOfData; CurrData)
	{
		unsigned char Bytes[]{mData[CurrData++], mData[CurrData++], mData[CurrData++], mData[CurrData++]};
		float Value;
		std::memcpy(&Value, Bytes, sizeof(float));
		FloatVect.push_back(Value);
	}

	return FloatVect;
}

std::vector<GLuint> Model::GetIndices(json aAccesor)
{
	std::vector<GLuint> Indices;

	int BuffViewIndex = aAccesor.value("bufferView", 0);
	int Count{aAccesor["count"]};
	int AccByteOffset = aAccesor.value("byteOffset", 0);
	int Type{aAccesor["componentType"]};

	json BufferView{mJSON["bufferViews"][BuffViewIndex]};
	int ByteOffset = BufferView.value("byteOffset", 0);

	int BeginOfData = ByteOffset + AccByteOffset;
	if (Type == 5125)
	{
		for (int CurrData {BeginOfData}; CurrData < ByteOffset + AccByteOffset + Count * 4; CurrData)
		{
			unsigned char Bytes[]{mData[CurrData++], mData[CurrData++], mData[CurrData++], mData[CurrData++]};
			unsigned int Value;
			std::memcpy(&Value, Bytes, sizeof(unsigned int));
			Indices.push_back(Value);
		}
	}
	else if (Type == 5123)
	{
		for (int CurrData {BeginOfData}; CurrData < ByteOffset + AccByteOffset + Count * 2; CurrData)
		{
			unsigned char Bytes[]{mData[CurrData++], mData[CurrData++]};
			unsigned short Value;
			std::memcpy(&Value, Bytes, sizeof(unsigned short));
			Indices.push_back((GLuint)Value);
		}
	}
	else if (Type == 5122)
	{
		for (int CurrData {BeginOfData}; CurrData < ByteOffset + AccByteOffset + Count * 2; CurrData)
		{
			unsigned char Bytes[]{mData[CurrData++], mData[CurrData++]};
			short Value;
			std::memcpy(&Value, Bytes, sizeof(short));
			Indices.push_back((GLuint)Value);
		}
	}

	return Indices;
}

std::vector<Texture> Model::GetTextures()
{
	std::vector<Texture> Textures;

	std::string FileStr {std::string(mFile)};
	std::string FileDir {FileStr.substr(0, FileStr.find_last_of('/') + 1)};

	unsigned int Unit = 0U;
	for (unsigned int ImageIndex {0U}; ImageIndex < mJSON["images"].size(); ImageIndex++)
	{
		std::string TexPath {mJSON["images"][ImageIndex]["uri"]};
		bool skip = false;
		
		for (unsigned int TextureIndex {0U}; TextureIndex < mLoadedTexNames.size(); TextureIndex++)
		{
			if(mLoadedTexNames[TextureIndex] == TexPath)
			{
				Textures.push_back(mLoadedTex[TextureIndex]);
				skip = true;
				break;
			}
		}

		if(!skip)
		{
			if (TexPath.find("baseColor") != std::string::npos)
			{
				Texture DiffTex {(FileDir + TexPath).c_str(), "diffuse", Unit++};
				Textures.push_back(DiffTex);
				mLoadedTex.push_back(DiffTex);
				mLoadedTexNames.push_back(TexPath);
			}
			else if (TexPath.find("metalicRoughness") != std::string::npos)
			{
				Texture SpecTex {(FileDir + TexPath).c_str(), "specular", Unit++};
				Textures.push_back(SpecTex);
				mLoadedTex.push_back(SpecTex);
				mLoadedTexNames.push_back(TexPath);
			}
		}
	}
	
	return Textures;
}

std::vector<glm::vec2> Model::GroupFloatsInVec2(std::vector<float> aFloatVect)
{
	std::vector<glm::vec2> Vectors;
	for (int VecIndex{0}; VecIndex < aFloatVect.size(); VecIndex)
	{
		Vectors.push_back(glm::vec2(aFloatVect[VecIndex++], aFloatVect[VecIndex++]));
	}
	return Vectors;
}

std::vector<glm::vec3> Model::GroupFloatsInVec3(std::vector<float> aFloatVect)
{
	std::vector<glm::vec3> Vectors;
	for (int VecIndex {0}; VecIndex < aFloatVect.size(); VecIndex)
	{
		Vectors.push_back(glm::vec3(aFloatVect[VecIndex++], aFloatVect[VecIndex++], aFloatVect[VecIndex++]));
	}
	return Vectors;
}

std::vector<glm::vec4> Model::GroupFloatsInVec4(std::vector<float> aFloatVect)
{
	std::vector<glm::vec4> Vectors;
	for (unsigned int VecIndex {0U}; VecIndex < aFloatVect.size(); VecIndex)
	{
		Vectors.push_back(glm::vec4(aFloatVect[VecIndex++], aFloatVect[VecIndex++], aFloatVect[VecIndex++], aFloatVect[VecIndex++]));
	}
	return Vectors;
}

std::vector<Vertex> Model::AssembleVertices(std::vector<glm::vec3> aPositions, std::vector<glm::vec3> aNormals, std::vector<glm::vec3> aColors,
	std::vector<glm::vec2> aTexUVs)
{
	std::vector<Vertex> Vertices;
	for (int VertexIndex {0}; VertexIndex < aPositions.size(); VertexIndex++)
	{
		Vertices.push_back(Vertex{aPositions[VertexIndex], aColors[VertexIndex], aNormals[VertexIndex], aTexUVs[VertexIndex]});
	}
	return Vertices;
}

void Model::TraverseNode(unsigned aNextNode, glm::mat4 aMatrix)
{
	json Node = mJSON["nodes"][aNextNode];

	glm::vec3 Translation = glm::vec3(0.0f);
	if (Node.find("translation") != Node.end())
	{
		float TransValues[3];
		for (unsigned int TransIndex {0U}; TransIndex < Node["translation"].size(); TransIndex++)
			TransValues[TransIndex] = Node["translation"][TransIndex];
		Translation = glm::make_vec3(TransValues) + mPosition;
	}
	
	glm::quat Rotation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
	if (Node.find("rotation") != Node.end())
	{
		float RotValues[4] =
		{ // X,y,z,w to w,x,y,z
			Node["rotation"][3],
			Node["rotation"][0],
			Node["rotation"][1],
			Node["rotation"][2]
		};
		Rotation = glm::make_quat(RotValues) + mRotation;
	}
	
	glm::vec3 Scale = glm::vec3(1.0f);
	if (Node.find("scale") != Node.end())
	{
		float ScaleValues[3];
		for (unsigned int ScaleIndex {0U}; ScaleIndex < Node["scale"].size(); ScaleIndex++)
			ScaleValues[ScaleIndex] = Node["scale"][ScaleIndex];
		Scale = glm::make_vec3(ScaleValues) * mScale;
	}

	glm::mat4 MatNode = glm::mat4(1.0f);
	if (Node.find("matrix") != Node.end())
	{
		float MatValues[16];
		for (unsigned int MatIndex {0U}; MatIndex < Node["matrix"].size(); MatIndex++)
			MatValues[MatIndex] = Node["matrix"][MatIndex];
		MatNode = glm::make_mat4(MatValues);
	}


	glm::mat4 Trans = glm::mat4(1.0f);
	glm::mat4 Rot = glm::mat4(1.0f);
	glm::mat4 Sca = glm::mat4(1.0f);
	
	Trans = translate(Trans, Translation);
	Rot = mat4_cast(Rotation);
	Sca = scale(Sca, Scale);
	
	glm::mat4 matNextNode = aMatrix * MatNode * Trans * Rot * Sca;

	if (Node.find("mesh") != Node.end())
	{
		mTranslationMeshes.push_back(Translation);
		mRotationMeshes.push_back(Rotation);
		mScaleMeshes.push_back(Scale);
		mMatrixMeshes.push_back(matNextNode);

		LoadMesh(Node["mesh"]);
	}

	if (Node.find("children") != Node.end())
	{
		for (unsigned int ChildrenIndex {0U}; ChildrenIndex < Node["children"].size(); ChildrenIndex++)
			TraverseNode(Node["children"][ChildrenIndex], matNextNode);
	}
}

void Model::Draw(Shader& aShader, Camera& aCamera)
{
	for (unsigned int MeshIndex {0U}; MeshIndex < mMeshes.size(); MeshIndex++)
	{
		mMeshes[MeshIndex].Draw(aShader, aCamera, mMatrixMeshes[MeshIndex], mTranslationMeshes[MeshIndex], mRotationMeshes[MeshIndex], mScaleMeshes[MeshIndex]);
	}
}

