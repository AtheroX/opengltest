#include "Mesh.h"

Mesh::Mesh(std::vector<Vertex>& aVertices, std::vector<GLuint>& aIndices, std::vector<Texture>& aTextures)
	: mVertices(aVertices), mIndices(aIndices), mTextures(aTextures)
{
	mVao.Bind();
	VBO Vbo {aVertices};
	EBO Ebo {aIndices};
	
	//Links VBO to VAO and unbind them to prevent changes
	mVao.LinkAttrib(Vbo, 0, 3, GL_FLOAT, sizeof(Vertex), (void*)0); //aPos layout
	mVao.LinkAttrib(Vbo, 1, 3, GL_FLOAT, sizeof(Vertex), (void*)(3 * sizeof(float))); //aColor layout
	mVao.LinkAttrib(Vbo, 2, 3, GL_FLOAT, sizeof(Vertex), (void*)(6 * sizeof(float))); //aNormals layout
	mVao.LinkAttrib(Vbo, 3, 2, GL_FLOAT, sizeof(Vertex), (void*)(9 * sizeof(float))); //aTex layout

	mVao.Unbind();
	Vbo.Unbind();
	Ebo.Unbind();
}

void Mesh::Draw(Shader& aShader, Camera& aCamera, glm::mat4 aMatrix, glm::vec3 aTransition, glm::quat aRotation, glm::vec3 aScale)
{
	aShader.Activate();
	mVao.Bind();

	unsigned int NumDiffuse {0U}, numSpecular {0U};

	for (unsigned int TextureIndex {0U}; TextureIndex < mTextures.size(); TextureIndex++)
	{
		std::string Num, Type {mTextures[TextureIndex].mType};

		if (Type == "diffuse")
		{
			Num = std::to_string(NumDiffuse++);
		}
		else if (Type == "specular")
		{
			Num = std::to_string(numSpecular++);
		}
		
		mTextures[TextureIndex].TexUnit(aShader, (Type + Num).c_str(), TextureIndex);
		mTextures[TextureIndex].Bind();
	}

	aShader.SetUniVec3("camPos", aCamera.mPosition);
	aCamera.UniMatrix(aShader, "camMatrix");

	glm::mat4 Trans {1.0f}, Rot {1.0f}, Sca {1.0f};

	Trans = translate(Trans, aTransition);
	Rot = mat4_cast(aRotation);
	Sca = scale(Sca, aScale);

	aShader.SetUniMat4("translation", Trans);
	aShader.SetUniMat4("rotation", Rot);
	aShader.SetUniMat4("scale", Sca);
	aShader.SetUniMat4("model", aMatrix);
	
	glDrawElements(GL_TRIANGLES, mIndices.size(), GL_UNSIGNED_INT, 0);
}
