#ifndef MESH_CLASS_H
#define MESH_CLASS_H

#include <string>

#include "../Buffers/VAO.h"
#include "../Buffers/EBO.h"
#include "../Camera/Camera.h"
#include "../Texture.h"

class Mesh
{
public:
	Mesh(std::vector<Vertex>& aVerices, std::vector<GLuint>& aIndices, std::vector<Texture>& aTextures);

	void Draw(Shader& aShader, Camera& aCamera,
		glm::mat4 aMatrix = glm::mat4(1.0f),
		glm::vec3 aTransition = glm::vec3(0.0f),
		glm::quat aRotation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f),
		glm::vec3 aScale = glm::vec3(1.0f));


	std::vector<Vertex> mVertices;
	std::vector<GLuint> mIndices;
	std::vector<Texture> mTextures;

	VAO mVao;
};


#endif // !MESH_CLASS_H
