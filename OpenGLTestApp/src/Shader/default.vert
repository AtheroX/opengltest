#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec3 aNormals;
layout (location = 3) in vec2 aTex;

out vec3 outPos;
out vec3 outColor;
out vec3 outNormals;
out vec2 outTex;

uniform mat4 camMatrix;
uniform mat4 model;

uniform mat4 translation;
uniform mat4 rotation;
uniform mat4 scale;

void main() 
{
	outPos = vec3(model * translation * -rotation * scale * vec4(aPos, 1.0f));
	gl_Position = camMatrix * vec4(outPos, 1.0f);

	outColor = aColor;
	outTex = mat2(0.0, -1.0, 1.0, 0.0) * aTex;
	outNormals = aNormals;
}