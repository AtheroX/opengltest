#include "ShaderClass.h"
#include <glm/gtc/type_ptr.hpp>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cerrno>
#include <direct.h> // _getcwd

#include "../AtheroXUtils.h"

std::string get_working_path() {
	char cwd[1024];
	if (_getcwd(cwd, sizeof(cwd)) != NULL)
		return std::string(cwd);
	else
		return std::string("");
}

std::string GetFileContents(const char* aFilename)
{
	std::string Path = get_working_path().append("/").append(aFilename);
	std::ifstream In(Path, std::ios::binary);
	if(In)
	{
		std::stringstream buffer;
		buffer << In.rdbuf();
		In.close();
		return buffer.str();
	}
	std::cout << "[GETFILE] Error getting file " << Path << "\n";
	throw(errno);
}

Shader::Shader(const std::string& aShaderPath)
{
	CreateShader(ParseShader(aShaderPath));
}

Shader::Shader(sShaderProgramSource aShaderProgram)
{
	std::string VertCodeStr {GetFileContents(aShaderProgram.VertexFile.c_str())};
	std::string FragCodeStr {GetFileContents(aShaderProgram.FragmentFile.c_str())};

	CreateShader({VertCodeStr, FragCodeStr});
}

sShaderProgramSource Shader::ParseShader(const std::string& aShader)
{
	std::ifstream Stream(aShader);

	enum class ShaderType
	{
		NONE= -1, VERTEX, FRAGMENT
	};

	std::string Line;
	std::stringstream StrBuffer[2];
	ShaderType Type = ShaderType::NONE;
	while(getline(Stream, Line))
	{
		if(Line.find("#shader") != std::string::npos)
		{
			if(Line.find("vertex") != std::string::npos)
			{
				Type = ShaderType::VERTEX;
			}
			else if(Line.find("fragment") != std::string::npos)
			{
				Type = ShaderType::FRAGMENT;
			}
		}
		else
		{
			StrBuffer[(int)Type] << Line << "\n";
		}
	}
	return {StrBuffer[0].str(), StrBuffer[1].str()};
}

void Shader::CreateShader(sShaderProgramSource aShader) {
	const char* vertCode = aShader.VertexFile.c_str();
	const char* freagCode = aShader.FragmentFile.c_str();

	//Create VertexShader and bind to Source
	GLuint VertShader = glCreateShader(GL_VERTEX_SHADER);
	GLCall(glShaderSource(VertShader, 1, &vertCode, nullptr));
	GLCall(glCompileShader(VertShader));
	ErrorCheck(VertShader, "VERTEX");

	//Create FragmentShader and bind to Source
	GLuint FragShader = glCreateShader(GL_FRAGMENT_SHADER);
	GLCall(glShaderSource(FragShader, 1, &freagCode, nullptr));
	GLCall(glCompileShader(FragShader));
	ErrorCheck(FragShader, "FRAGMENT");

	//Attatch both fragment and vertex to same program
	mID = glCreateProgram();
	GLCall(glAttachShader(mID, VertShader));
	GLCall(glAttachShader(mID, FragShader));
	GLCall(glLinkProgram(mID));
	ErrorCheck(mID, "PROGRAM");

	//As we have the program, vert and frag are useless
	GLCall(glDeleteShader(VertShader));
	GLCall(glDeleteShader(FragShader));
}

void Shader::Activate()
{
	GLCall(glUseProgram(mID));
}

void Shader::Delete()
{
	GLCall(glDeleteProgram(mID));
}

void Shader::SetUniBool(const char* aUniName, bool aValue)
{
	GLCall(glUniform1i(GetUniformLocation(aUniName), (int)aValue));
}

void Shader::SetUniInt(const char* aUniName, int aValue)
{
	GLCall(glUniform1i(GetUniformLocation(aUniName), aValue));
}

void Shader::SetUniFloat(const char* aUniName, float aValue)
{
	GLCall(glUniform1f(GetUniformLocation(aUniName), aValue));
}

void Shader::SetUniVec2(const char* aUniName, glm::vec2 aValue)
{
	GLCall(glUniform2fv(GetUniformLocation(aUniName), 1, glm::value_ptr(aValue)));
}

void Shader::SetUniVec3(const char* aUniName, glm::vec3 aValue)
{
	GLCall(glUniform3f(GetUniformLocation(aUniName), aValue.x, aValue.y, aValue.z));
}

void Shader::SetUniVec4(const char* aUniName, glm::vec4 aValue)
{
	GLCall(glUniform4f(GetUniformLocation(aUniName), aValue.x, aValue.y, aValue.z, aValue.w));
}

void Shader::SetUniMat4(const char* aUniName, glm::mat4 aValue)
{
	GLCall(glUniformMatrix4fv(GetUniformLocation(aUniName), 1, GL_FALSE, glm::value_ptr(aValue)));
}

void Shader::ErrorCheck(unsigned int aShader, const char* aType)
{
	GLint HasCompiled;
	char Log[1024];

	glGetShaderiv(aShader, GL_COMPILE_STATUS, &HasCompiled);
	if(HasCompiled == GL_FALSE)
	{
		glGetShaderInfoLog(aShader, 1024, nullptr, Log);
		std::cout << (aType != "PROGRAM" ? "Shader COMPILATION error: " : "Shader LINKING error: ") << aType << "\n" << Log << "\n";
	}
}

GLint Shader::GetUniformLocation(const char* aUniform) const
{
	if(mUniformCache.find(aUniform) != mUniformCache.end())
		return mUniformCache[aUniform];

	GLint Location = glGetUniformLocation(mID, aUniform);
	mUniformCache[aUniform] = Location;
	return Location;
}
