﻿#shader vertex
#version 330 core

layout (location = 0) in vec3 aPos;

uniform mat4 model;
uniform mat4 camMatrix;

uniform mat4 translation;
uniform mat4 rotation;
uniform mat4 scale;

void main()
{
	gl_Position = camMatrix * vec4(vec3(model * translation * -rotation * scale * vec4(aPos, 1.0f)), 1.0f);
}


#shader fragment
#version 330 core

out vec4 outColor;

uniform vec3 lightColor;

void main()
{
	outColor = vec4(lightColor, 1.0);
}