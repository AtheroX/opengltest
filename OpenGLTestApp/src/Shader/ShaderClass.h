#ifndef SHADER_CLASS_H
#define SHADER_CLASS_H

#include <glad/glad.h>
#include <string>
#include <unordered_map>
#include <glm/glm.hpp>


std::string GetFileContents(const char* aFileName);

struct sShaderProgramSource
{
	std::string VertexFile;
	std::string FragmentFile;
};

class Shader
{
public:
	Shader(const std::string& aShaderPath);
	Shader(sShaderProgramSource aShaderProgram);

	void Activate();
	void Delete();

	void CreateShader(sShaderProgramSource aShader);
	sShaderProgramSource ParseShader(const std::string& aShader);
	
	//UNIFORM
	void SetUniBool(const char* aUniName, bool aValue);
	void SetUniInt(const char* aUniName, int aValue);
	void SetUniFloat(const char* aUniName, float aValue);
	void SetUniVec2(const char* aUniName, glm::vec2 aValue);
	void SetUniVec3(const char* aUniName, glm::vec3 aValue);
	void SetUniVec4(const char* aUniName, glm::vec4 aValue);
	void SetUniMat4(const char* aUniName, glm::mat4 aValue);


	GLuint mID;
	mutable std::unordered_map<std::string, GLint> mUniformCache;

private:
	void ErrorCheck(unsigned int aShader, const char* aType);

	GLint GetUniformLocation(const char* aUniform) const;
};


#endif
