#version 330 core

in vec3 outColor;
in vec2 outTex;
in vec3 outNormals;
in vec3 outPos;

out vec4 FragColor;

uniform sampler2D diffuse0;	//Base
uniform sampler2D specular0; //Spec

uniform vec3 lightColor;
uniform vec3 lightPos;
uniform vec3 camPos;


const float specShininess = 16;


vec4 PointLight()
{
	vec3 lightVector = lightPos - outPos;
	float distToLight = length(lightVector);
	float quadratic = 1.0;
	float linear = 0.04;
	float intensOverDist = 1.0f / (quadratic * distToLight * distToLight + linear + 1.0f);
	
	//ambient
	float ambientLight = 0.2f;
	vec3 ambient = ambientLight * lightColor;

	//Diff
	vec3 normal = normalize(outNormals);
	vec3 lightDir = normalize(lightVector);
	float diffuseLight = max(dot(normal, lightDir), 0.0f);
	vec3 diffuse = diffuseLight * lightColor * intensOverDist;

	//Spec
	float specularIntensity = 0.5f;
	vec3 viewDir = normalize(camPos - outPos);
	vec3 reflectVec = reflect(-lightDir, normal);
	float specAmount = pow(max(dot(viewDir, reflectVec), 0.0), specShininess);
	float specularLight = specAmount * specularIntensity * 3;
	vec3 specular = texture(specular0, outTex).r * specularLight * lightColor * intensOverDist;

	return vec4(ambient+diffuse+specular, 1.0);
}

vec4 DirectLight()
{
	vec3 lightDirection = vec3(1.0f, 1.0f, 0.0f);

	//ambient
	float ambientLight = 0.2f;
	vec3 ambient = ambientLight * lightColor;

	//Diff
	vec3 normal = normalize(outNormals);
	vec3 lightDir = normalize(lightDirection);
	float diffuseLight = max(dot(normal, lightDir), 0.0f);
	vec3 diffuse = diffuseLight * lightColor;

	//Spec
	float specularIntensity = 0.5f;
	vec3 viewDir = normalize(camPos - outPos);
	vec3 reflectVec = reflect(-lightDir, normal);
	float specAmount = pow(max(dot(viewDir, reflectVec), 0.0), specShininess);
	float specularLight = specAmount * specularIntensity;
	vec3 specular = texture(specular0, outTex).r * specularLight * lightColor;

	return vec4(ambient+diffuse+specular, 1.0);
}

vec4 SpotLight()
{
	vec3 lightVector = lightPos - outPos;
	vec3 lightDir = normalize(lightPos - outPos);

	float outerConeCos = 0.9f;
	float innerConeCos = 0.95f;
	vec3 spotLightDir = vec3(0.0f, -1.0f, 0.0f);
	float angle = dot(spotLightDir, -lightDir);
	float intensity = clamp((angle - outerConeCos) / (innerConeCos - outerConeCos), 0.0f, 1.0f);


	//ambient
	float ambientLight = 0.2f;
	vec3 ambient = ambientLight * lightColor;

	//Diff
	vec3 normal = normalize(outNormals);
	float diffuseLight = max(dot(normal, lightDir), 0.0f);
	vec3 diffuse = diffuseLight * lightColor * intensity;

	//Spec
	float specularIntensity = 0.5f;
	vec3 viewDir = normalize(camPos - outPos);
	vec3 reflectVec = reflect(-lightDir, normal);
	float specAmount = pow(max(dot(viewDir, reflectVec), 0.0), specShininess);
	float specularLight = specAmount * specularIntensity;
	vec3 specular = texture(specular0, outTex).r * specularLight * lightColor * intensity;

	return vec4(ambient+diffuse+specular, 1.0);
}


void main()
{
	//Texture
	vec4 texture0 = texture(diffuse0, outTex) * vec4(lightColor, 1.0);

	FragColor = texture0 * PointLight();
}