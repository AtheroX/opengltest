#include "ModelImporter/Model.h"
#include <GLFW/glfw3.h>
// #include "Cosas.h"

const unsigned int WHEIGHT {800U}, WWIDTH {800U};
const float NEAR_PLANE {0.02f}, FAR_PLANE {100.0f};

const char* VERSION {"0.0.6"};

int main()
{
	glfwInit();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	//Create window
	GLFWwindow* Window = glfwCreateWindow(WWIDTH, WHEIGHT, std::string("OpenGLTest v").append(VERSION).c_str(), nullptr, nullptr);
	if(Window == nullptr)
	{
		std::cout << "FAIL\n";
		glfwTerminate();
		return -1;
	}
	//Loads windows as current context
	glfwMakeContextCurrent(Window);

	//Hide cursor and center mouse
	glfwSetInputMode(Window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	glfwSetCursorPos(Window, (WWIDTH / 2), (WHEIGHT / 2));

	//loads glad lib into OGL
	gladLoadGL();
	//Says the bounds of the gl.
	glViewport(0, 0, WWIDTH, WHEIGHT);


	//Create all objects and Shader with defaults
	Shader ShaderProgram {sShaderProgramSource {"src/Shader/default.vert", "src/Shader/default.frag"}};

	Shader LightProgram {"src/Shader/light.shader"};
	glm::vec3 LightColor{1.0f};
	glm::vec3 LightPos{0.f,10.f,0.f};
	Model LightModel {"res/models/VertexColor/BoxVertexColors.gltf"};

	LightProgram.Activate();
	LightProgram.SetUniVec3("lightColor", LightColor);
	ShaderProgram.Activate();
	ShaderProgram.SetUniVec3("lightColor", LightColor);
	ShaderProgram.SetUniVec3("lightPos", LightPos);


	Camera Camera {WWIDTH, WHEIGHT, glm::vec3{0.0f, 0.0f, 2.0f}, 120.0f};

	Model BoxModel {"res/models/VertexColor/BoxVertexColors.gltf"};
	BoxModel.Scale(glm::vec3{1.f});

	glEnable(GL_DEPTH_TEST);

	char buff[100];

	while(!glfwWindowShouldClose(Window))
	{
		//Loads a color into buffer
		glClearColor(0.07f, 0.13f, 0.17f, 1.0f);
		//Clears back buffer and loads the color is in buffer rn
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		Camera.Inputs(Window);
		Camera.UpdateMatrix(NEAR_PLANE, FAR_PLANE);

		BoxModel.Draw(ShaderProgram, Camera);
		LightModel.Draw(LightProgram, Camera);

		BoxModel.Translate(glm::vec3(0.f, 0.008f, 0.f));
		BoxModel.Rotate(glm::vec3(1.f,0.f,1.f));
		LightModel.Scale(glm::vec3(1.002f));
		
		snprintf(buff, sizeof(buff), std::string("OpenGLTest v").append(VERSION).append(" - Pos: %Fx | %Fy | %Fz").c_str(), Camera.mPosition.x, Camera.mPosition.y, Camera.mPosition.z);
		glfwSetWindowTitle(Window, buff);

		// LightMesh.Draw(LightProgram, Camera);

		//Swaps the back and front buffer (white->Loaded color)
		glfwSwapBuffers(Window);

		glfwPollEvents();
	}

	//Clears
	ShaderProgram.Delete();
	// LightProgram.Delete();

	glfwDestroyWindow(Window);
	std::cout << "Bye\n";
	glfwTerminate();
	return 0;
}
