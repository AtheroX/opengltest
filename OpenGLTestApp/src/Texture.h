#ifndef TEXTURE_CLASS_H
#define TEXTURE_CLASS_H

#include <glad/glad.h>
#include <stb/stb_image.h>

#include "Shader/ShaderClass.h"

class Texture
{
public:
	Texture(const char* apImage, const char* aTextType, GLuint aSlot);

	void TexUnit(Shader& aShader, const char* apUniform, GLuint aUnit);

	void Bind();

	void Unbind();

	void Delete();


	GLuint mID;
	const char* mType;
	GLuint mUnit;
};

#endif