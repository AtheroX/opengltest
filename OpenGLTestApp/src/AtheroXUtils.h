﻿#ifndef ATHUTILS
#define ATHUTILS

#include <iostream>

#define ASSERT(x) if (!(x)) __debugbreak()
#define GLCall(x) GLClearError();\
x;\
ASSERT(GLLog(#x, __FILE__, __LINE__))

static void GLClearError(){ while(glGetError() != GL_NO_ERROR); }
static bool GLLog(const char* aFunc, const char* aFile, int aLine)
{
	bool AllGood {true};
	while (GLenum err = glGetError())
	{
		std::cout << "[OpenGL Error] ("<< err << "): "<< aFunc << " on " << aFile<<":L"<<aLine << std::endl;
		AllGood = false;
	}
	return AllGood;
}

#endif