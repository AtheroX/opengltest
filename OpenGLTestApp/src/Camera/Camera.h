#ifndef CAMERA_CLASS_H
#define CAMERA_CLASS_H

#include "../Shader/ShaderClass.h"

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/vector_angle.hpp>



class Camera
{
public:
	Camera(int aWidth, int aHeight, glm::vec3 aPos, float aFOV);

	void UniMatrix(Shader& aShader, const char* aUniform);
	void UpdateMatrix(float aNear, float aFar);
	void Inputs(GLFWwindow* apWindow);


	glm::vec3 mPosition;
	glm::vec3 mForward{0.0f, 0.0f, -1.0f};
	glm::vec3 mUpward{0.0f, 1.0f, 0.0f};
	glm::mat4 mCamMatrix{1.0f};

	float mSpeed{0.1f};
	float mSensitivity{100.f};
	float mFOV{90.0f};
	int   mWidth;
	int   mHeight;

	bool mMouseVisible{false};
	bool mMouseEscPress{false};
};


#endif
