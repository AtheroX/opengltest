#include <iostream>

#include "Camera.h"

Camera::Camera(int aWidth, int aHeight, glm::vec3 aPos, float aFOV)
	: mPosition(aPos), mWidth(aWidth), mHeight(aHeight), mFOV(aFOV) {}

void Camera::UpdateMatrix(float aNear, float aFar)
{
	glm::mat4 View{1.0f}, Proj{1.0f};

	View = glm::lookAt(mPosition, mPosition + mForward, mUpward);
	Proj = glm::perspective(glm::radians(mFOV), (float)(mWidth / mHeight), aNear, aFar);

	mCamMatrix = Proj * View;
}

void Camera::UniMatrix(Shader& aShader, const char* aUniform)
{
	aShader.SetUniMat4(aUniform, mCamMatrix);
}

void Camera::Inputs(GLFWwindow* apWindow)
{
	if (glfwGetKey(apWindow, GLFW_KEY_W) == GLFW_PRESS)
	{
		mPosition += mSpeed * mForward;
	}
	else if (glfwGetKey(apWindow, GLFW_KEY_S) == GLFW_PRESS)
	{
		mPosition -= mSpeed * mForward;
	}

	if (glfwGetKey(apWindow, GLFW_KEY_D) == GLFW_PRESS)
	{
		mPosition += mSpeed * glm::normalize(glm::cross(mForward, mUpward));
	}
	else if (glfwGetKey(apWindow, GLFW_KEY_A) == GLFW_PRESS)
	{
		mPosition -= mSpeed * glm::normalize(glm::cross(mForward, mUpward));
	}

	if (glfwGetKey(apWindow, GLFW_KEY_SPACE) == GLFW_PRESS)
	{
		mPosition += mSpeed * mUpward;
	}
	else if (glfwGetKey(apWindow, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
	{
		mPosition -= mSpeed * mUpward;
	}

	if (glfwGetKey(apWindow, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
	{
		mSpeed = 0.1f;
	}
	else if (glfwGetKey(apWindow, GLFW_KEY_LEFT_ALT) == GLFW_PRESS)
	{
		mSpeed = 0.005f;
	}
	else if (glfwGetKey(apWindow, GLFW_KEY_LEFT_ALT) == GLFW_RELEASE ||
		glfwGetKey(apWindow, GLFW_KEY_LEFT_CONTROL) == GLFW_RELEASE)
	{
		mSpeed = 0.02f;
	}

	if (glfwGetKey(apWindow, GLFW_KEY_ESCAPE) == GLFW_PRESS && !mMouseEscPress)
	{
		mMouseEscPress = true;
		mMouseVisible = !mMouseVisible;
		glfwSetCursorPos(apWindow, (mWidth / 2), (mHeight / 2));
		glfwSetInputMode(apWindow, GLFW_CURSOR, mMouseVisible ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_HIDDEN);
	}
	if (glfwGetKey(apWindow, GLFW_KEY_ESCAPE) == GLFW_RELEASE)
	{
		mMouseEscPress = false;
	}

	if (!mMouseVisible)
	{
		double MouseX, MouseY;
		glfwGetCursorPos(apWindow, &MouseX, &MouseY);

		//0,0 is not center
		float RotX{mSensitivity * (float)(MouseY - (mHeight / 2)) / mHeight};
		float RotY{mSensitivity * (float)(MouseX - (mHeight / 2)) / mHeight};

		glm::vec3 NewForward{glm::rotate(mForward, glm::radians(-RotX), glm::normalize(glm::cross(mForward, mUpward)))};
		if (!(glm::angle(NewForward, mUpward) <= glm::radians(5.0f) || glm::angle(NewForward, -mUpward) <= glm::radians(5.0f)))
		{
			mForward = NewForward;
		}
		mForward = glm::rotate(mForward, glm::radians(-RotY), mUpward);
		glfwSetCursorPos(apWindow, (mWidth / 2), (mHeight / 2));
	}

	if (glfwGetKey(apWindow, GLFW_KEY_E) == GLFW_PRESS)
	{
		mFOV += 1.0f / 6.0f;
		std::cout << "FOV " << mFOV << "\n";
	}
	else if (glfwGetKey(apWindow, GLFW_KEY_Q) == GLFW_PRESS)
	{
		mFOV -= 1.0f / 6.0f;
		std::cout << "FOV " << mFOV << "\n";
	}
}
